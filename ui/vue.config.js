module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
    ? '/bgg/'
    : '/',
    pages: {
        index: {
          entry: 'src/main.js',
          title: 'bggtrader'
        }
      }
  }