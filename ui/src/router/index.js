import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Trades from '@/views/Trades.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/trades/:username',
    name: 'Trades',
    component: Trades,
    meta: {
      title: 'what'
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  linkActiveClass: 'is-active',
  base: process.env.BASE_URL,
  routes
})

export default router
