const axios = require('axios')
const baseUrl = process.env.VUE_APP_API_ENDPOINT

export default {
    async getGame(id) {
        return await axios.get(baseUrl+'thing/'+id)
    },
    async getUser(id) {
        return await axios.get(baseUrl+'user/'+id)
    }
}