// Create clients and set shared const values outside of the handler.

// Get the DynamoDB table name from environment variables
const tableName = process.env.CACHE_TABLE;

// Create a DocumentClient that represents the query to add an item
const dynamodb = require('aws-sdk/clients/dynamodb');
const docClient = new dynamodb.DocumentClient();
const got = require('got')
const { Parser } = require('xml2js')
const xml2js = new Parser()

/**
 * A simple example includes a HTTP get method to get one item by id from a DynamoDB table.
 */
exports.getGameInfoHandler = async (event) => {
  if (event.httpMethod !== 'GET') {
    throw new Error(`getMethod only accept GET method, you tried: ${event.httpMethod}`);
  }
  // All log statements are written to CloudWatch


  // Get id from pathParameters from APIGateway because of `/{id}` at template.yml
  const id = event.pathParameters.id;
  let gameInfo

  try {
    const getParams = {
      TableName: tableName,
      Key: { id:  `game:${id}` }
    }

    console.log(getParams)
    const getResp = await docClient.get(getParams).promise()
    gameInfo = JSON.parse(getResp.Item.gameInfo)
    console.log(`Retireved game ${id} from cache...`)
  } catch (err) {
    console.log(`Game ${id} not cached. Pulling from BGGAPI...`)
    console.log(err)
  }

  if(!gameInfo) {
    try {
      const gameXml = await got(`https://boardgamegeek.com/xmlapi2/thing?id=${id}&stats=1`)
      gameInfo = (await xml2js.parseStringPromise(gameXml.body)).items.item[0]

      var params = {
        TableName : tableName,
        Item: {
          id: `game:${id}`,
          ttl: parseInt(Date.now() / 1000) + 604800,
          gameInfo: JSON.stringify(gameInfo)
        },
      };

      await docClient.put(params).promise();
    } catch (err) {
      console.log(`Ruh-roh: ${err.message}!`)
      console.log(err)
      gameInfo = err
    }
  }

  // All log statements are written to CloudWatch
  console.info(`response from: ${event.path}`);
  return {
    statusCode: 200,
    body: JSON.stringify(gameInfo),
    headers: {
      "Access-Control-Allow-Headers":
      "Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
  "Access-Control-Allow-Methods":
      "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
  "Access-Control-Allow-Origin":
      "*"
    }
  };
}
