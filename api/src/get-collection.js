const docClient = require('./dynamodb')
const bggClient = require('./bggClient')
const got = require('got')
const { Parser } = require('xml2js')
const xml2js = new Parser()
const { isValidUsername, respond } = require("./utils")

exports.getCollectionHandler = async (event) => {
  if (event.httpMethod !== 'GET') {
    throw new Error(`getMethod only accept GET method, you tried: ${event.httpMethod}`);
  }

  const id = event.pathParameters.user;
  console.log(`userid: ${id}`)
  if (!isValidUsername(id)) {
    console.log('Not a valid bgg username...')
    return respond(400, {
      message: 'This is not a valid bgg username'
    })
  }

  let userInfo
  try {
    console.log('Trying to get cached data from dynamodb')
    userInfo = await docClient.getUser(id)
    console.log(`Retireved user ${id} from cache...`)
  } catch (err) {
    console.log(err)
  }

  if(!userInfo) {
    console.log(`User ${id} not cached. Pulling from BGGAPI...`)
    try {
      let userXml = await bggClient.getUser(id)
      if (userXml.statusCode !== 200) {
        console.log(`BGG returned a ${userXml.statusCode}`)
        return respond(500, {
          message: `BGG returned a ${userXml.statusCode}`
        })
      }

      userInfo = await xml2js.parseStringPromise(userXml.body)
      
      console.log('Got some user info, caching in DynamoDB')
      await docClient.putUser(id, userInfo)
    } catch (err) {
      console.log(`Ruh-roh: ${err.message}!`)
      console.log(err)
      userInfo = err
    }
  }
  
  console.info('Well we did it. We are going to return...something.');
  return respond(200, userInfo)
}
