const dynamodb = require('aws-sdk/clients/dynamodb');
const docClient = new dynamodb.DocumentClient();
const tableName = process.env.CACHE_TABLE

module.exports = {
  async getUser(id) {
    const resp = await docClient.get({
      TableName: tableName,
      Key: { id: `user:${id}` }
    }).promise()

    if ('Item' in resp && 'ttl' in resp.Item) {
      if ((Date.now() / 1000) > resp.Item.ttl) {
        resp.Item = undefined
      }
    }

    return JSON.parse(resp.Item.userInfo)
  },
  async putUser(id, info) {
    return await docClient.put({
      TableName : tableName,
      Item: {
        id: `user:${id}`,
        ttl: parseInt(Date.now() / 1000) + 15000,
        userInfo: JSON.stringify(info)
      },
    }).promise();
  }
}