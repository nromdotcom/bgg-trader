module.exports = {
    isValidUsername(username) {
        return username.match(/^[A-Za-z]{1}[A-Za-z0-9_]{3,19}$/)
    },
    respond(status, body) {
        return {
            statusCode: status,
            body: JSON.stringify(body),
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
                "Access-Control-Allow-Origin": "*"
              }
        }
    },
    bggBaseUrl: 'https://boardgamegeek.com/xmlapi2'
}