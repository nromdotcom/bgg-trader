const got = require('got')
const bggBaseUrl = 'https://boardgamegeek.com/xmlapi2'

module.exports = {
  async getUser(id) {
    let userXml
    let attempts = 4
    do {
      userXml = await got(`${bggBaseUrl}/collection?username=${id}&trade=1`)
      if(userXml.statusCode === 202) {
        attempts -= 1
        console.log('Request queued...retrying in 500ms...')
        await new Promise((r) => {
          setTimeout(() => r(), 500)
        })
      } else if (userXml.statusCode !== 200) {
        console.log("Something terrible happened.")
      }
    } while (userXml.statusCode === 202 && attempts >= 0)

    return userXml
  }
}